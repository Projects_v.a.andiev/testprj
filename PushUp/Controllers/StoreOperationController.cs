﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PushUp.Model;
using PushUp.Model.Helpers;

namespace PushUp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StoreOperationController : ControllerBase
    {
        private readonly IRepositoryModel _repModel;
        private HttpHelper _httpclient;
        public StoreOperationController(IRepositoryModel repModel, HttpHelper httpclient  )
        {
            _repModel = repModel;
            _httpclient = httpclient;
        }

        /// <summary>
        /// Получить значение записи по навзванию приложения регистронезависимо 
        /// </summary>
        /// <remarks>
        /// 1
        /// </remarks>
        /// <returns></returns>
        // GET api/values/5      
        [HttpGet("GetRecord")]
        public IActionResult GetRecord(string nameApplication )
        {
            try
            {
                return Ok(_repModel.GetRecord(nameApplication));
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Поиск прилодения в Google Play и App Store
        /// </summary>
        /// <remarks>
        /// Пример
        /// Бесплатные игры
        /// </remarks>
        /// <param name="appName">Введите название приложения для его поиска</param>
        /// <returns></returns>        
        [HttpPost("SearchApplication")]
        public IActionResult SearchApplication(string appName)
        {
            try
            {
                return Ok(_repModel.SearchApplication(appName));
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }
      
    }
}
