﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PushUp.Model.ConcurrentDictionaryStorage
{

    public interface IConcurrentDictionaryAdapter
    {
        MobileProgram this[string key] { get; set; }
        IEnumerable<MobileProgram> Values { get; }
        bool TryGetValue(string key, out MobileProgram record);
        MobileProgram AddOrUpdate(string key, MobileProgram record, Func<string, MobileProgram, MobileProgram> p);
        bool TryRemove(string key, out MobileProgram value);
        bool TryAdd(string key, MobileProgram value);
        void Clear();
    }
}
