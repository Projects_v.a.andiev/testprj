﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PushUp.Model
{
    public interface IRepositoryModel
    {
        /// <summary>
        /// Добавление новой записи и возврат добавленой записи
        /// </summary>
        /// <param name="applicationName"></param>
        /// <returns></returns>
        List<MobileProgram> SearchApplication(string applicationName);

        /// <summary>
        /// Все записи которые есть в хранилище
        /// </summary>
        IEnumerable<MobileProgram> Records { get; }

        /// <summary>
        /// Получение записи по названию программы и возврат полученной записи
        /// </summary>
        /// <param name="applicationName"></param>
        /// <returns></returns>
        IEnumerable< MobileProgram> GetRecord(string applicationName);
    }
}
