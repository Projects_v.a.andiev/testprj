﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace PushUp.Model.Helpers
{
    public class HttpHelper
    {
        public  HttpClient ApiClient { get; set; } = new HttpClient();

        public  void InitializeClient()
        {
            ApiClient = new HttpClient();
            ApiClient.DefaultRequestHeaders.Accept.Clear();
            ApiClient.DefaultRequestHeaders.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
            ApiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<string> GetStorageHtml(string url)
        {
            return await ApiClient.GetStringAsync(url);
        }

        public async Task<List<string>> GetStorageHtmls(List<string> urls)
        {
            var urlsTasks = urls.AsParallel().Select(i =>
            {
                return ApiClient.GetStringAsync(i);
            });

            var htmls = await Task.WhenAll(urlsTasks);

            return htmls.ToList();
        }
    }
}
