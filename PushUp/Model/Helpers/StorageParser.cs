﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PushUp.Model.Helpers
{
    public class StorageParser
    {
        public List<MobileProgram> GooglePlayApplication(string html)
        {
            HtmlDocument doc = new HtmlDocument();
            List<MobileProgram> mobProgram = new List<MobileProgram>();
            doc.Load(new StringReader(html));

            var images = doc.DocumentNode.SelectNodes("//span[@class=\"yNWQ8e K3IMke buPxGf\"]/span/img");
            var nameApp = doc.DocumentNode.SelectNodes("//div[@class=\"WsMG1c nnK0zc\"]");
            var starsApp = doc.DocumentNode.SelectNodes("//div[@class=\"pf5lIe\"]/div");
            var idApplications = doc.DocumentNode.SelectNodes("//div[@class=\"wXUyZd\"]/a");

            for (int i = 0; i <= 2; i++)
            {

                var img = images[i].GetAttributeValue("data-src", "");
                var naApp = nameApp[i].GetAttributeValue("title", "");
                var stApp = starsApp[i].GetAttributeValue("aria-label", "");
                var idApp = idApplications[i].GetAttributeValue("href", "");                

                mobProgram.Add(FillingMobileProgram(img, stApp, naApp, idApp, "GooglePlay"));

            }

            return mobProgram;
        }

        public List<MobileProgram> AppStoreApplication(string htmlAppStore)
        {
            HtmlDocument doc = new HtmlDocument();
            List<MobileProgram> mobProgram = new List<MobileProgram>();
            doc.Load(new StringReader(htmlAppStore));

            var images = doc.DocumentNode.SelectNodes(
                "//picture[@class=\"we-artwork ember-view product-hero__artwork we-artwork--fullwidth we-artwork--ios-app-icon\"]/img");
            var nameApp = doc.DocumentNode.SelectNodes("//h1[@class=\"product-header__title app-header__title\"]");
            var ratingApp = doc.DocumentNode.SelectNodes(
                "//li[@class=\"product-header__list__item app-header__list__item--user-rating\"]/ul/li/figure");
            var idApplications = doc.DocumentNode.SelectNodes("//meta[@property=\"og:url\"]");



       
                var obj =  new
                {
                    img = images[0].GetAttributeValue("src", ""),
                    naApp = nameApp[0].FirstChild.InnerText.Trim(),
                    raApp = ratingApp[0].GetAttributeValue("aria-label", ""),
                    idApp = idApplications[0].GetAttributeValue("content", "")
                };

                mobProgram.Add(FillingMobileProgram(obj.img, obj.raApp, obj.naApp, obj.idApp, "AppStore"));

        

            return mobProgram;
        }

        public List<MobileProgram> ConcatAppStoreApps(List<string>  urlsApps )
        {
            var apps =  new List<MobileProgram>();
            foreach (var url in urlsApps)
            {
                apps.AddRange(AppStoreApplication(url));
            }

            return apps;
        }


        public List<string> GetTop3Urls(string html)
        {
            HtmlDocument doc = new HtmlDocument();
            List<string> urlList = new List<string>();
            doc.Load(new StringReader(html));

            var urls = doc.DocumentNode.SelectNodes("//section[@class=\"section apps chart-grid\"]/div/ul/li/a");          

            for (int i = 0; i <= 2; i++)
            {
                urlList.Add( urls[i].GetAttributeValue("href", ""));
            }

            return urlList;
        }      




        public MobileProgram FillingMobileProgram(string image, string appRating, string appName, string appId, string storeName)
        {
            return
            new MobileProgram
            {
                ApplicationName = appName,
                Image = image,
                ApplicationRating = appRating,
                ApplicationId = appId,
                StoreName = storeName
            };

        }



        public string GetSearchUrlGoogle(string desiredProgram)
        {

            desiredProgram = Regex.Replace(desiredProgram, @"\s+", "%20");
            return "https://play.google.com/store/search?q=" + desiredProgram + "&c=apps";
        }

        public string GetSearchUrlAppStore()
        {
            return "https://www.apple.com/ru/itunes/charts/free-apps/";
        }


        public List<MobileProgram> GetApplicationStore (List<MobileProgram> mobilePrograms)        {


            var mobilesApp = mobilePrograms;
            List<MobileProgram> apps = new List<MobileProgram>();

            foreach (var app in mobilesApp)
            {
                var newRecord = new MobileProgram
                {
                    ApplicationName = app.ApplicationName,
                    Image = app.Image,
                    ApplicationRating = app.ApplicationRating,
                    ApplicationId = app.ApplicationId,
                    StoreName = app.StoreName
                };
             
                apps.Add(app);
            }

            return apps;
        }



    }
}
