﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PushUp.Model
{
    [Serializable]
    public class MobileProgram
    {
        public string ApplicationId { get; set; }
        public string ApplicationName { get; set; }
        public string ApplicationRating { get; set; }
        public string Image { get; set; }
        public string StoreName { get; set; }
    }


    public class MobilePrograms
    {
        List<MobileProgram> Programs { set; get; }
    }
}
