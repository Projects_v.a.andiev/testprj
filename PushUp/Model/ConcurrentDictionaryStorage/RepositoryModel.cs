﻿using PushUp.Model.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PushUp.Model.ConcurrentDictionaryStorage
{
    public class RepositoryModel : IRepositoryModel
    {
        private IConcurrentDictionaryAdapter _storage { get; }
        private HttpHelper _httpHelpers;
        private StorageParser _storageParser;
        public RepositoryModel(IConcurrentDictionaryAdapter storage, HttpHelper aPIHelpers, StorageParser storageParser)
        {
            _storage = storage;
            _httpHelpers = aPIHelpers;
            _storageParser = storageParser;
        }

        public IEnumerable<MobileProgram> Records => _storage.Values;

        public IEnumerable<MobileProgram> GetRecord(string nameApplication)
        {
            try
            {
                return _storage.Values.Where(x => x.ApplicationName.ToLower().Contains(nameApplication.ToLower()));
            }
            catch (KeyNotFoundException)
            {
                throw new ArgumentException($"Программа с названием {nameApplication} не найдена.");
            }
        }

        public List<MobileProgram> SearchApplication(string applicationName)
        {

            var appsGoogle = GetGoogleApps(applicationName);

            var appsAppStore = GetAppStoreApplication();

            var task1 = Task.Factory.StartNew(() => GetGoogleApps(applicationName));
            var task2 = Task.Factory.StartNew(() => GetAppStoreApplication());
            Task.WaitAll(task1, task2);

            appsGoogle = task1.Result;
            appsAppStore = task2.Result;

            var apps = new List<MobileProgram>();
            apps.AddRange(appsGoogle);
            apps.AddRange(appsAppStore);
            return apps;

        }

        public List<MobileProgram>  GetAppStoreApplication ()
        {
            var htmlFreeStoreApps = _httpHelpers.GetStorageHtml(_storageParser.GetSearchUrlAppStore()).GetAwaiter().GetResult();
            var urlsTop3AppStoreProgramms = _storageParser.GetTop3Urls(htmlFreeStoreApps);
            var htmlTop3AppStoreProgram = _httpHelpers.GetStorageHtmls(urlsTop3AppStoreProgramms).GetAwaiter().GetResult();
            var appsAppStore = _storageParser.ConcatAppStoreApps(htmlTop3AppStoreProgram);
            return appsAppStore;
        }

        public List<MobileProgram> GetGoogleApps(string applicationName)
        {
            var htmlGoogleStore = _httpHelpers.GetStorageHtml(_storageParser.GetSearchUrlGoogle(applicationName)).GetAwaiter().GetResult();
            var appsGoogle = _storageParser.GetApplicationStore(_storageParser.GooglePlayApplication(htmlGoogleStore));
            return appsGoogle;
        }

        public List<MobileProgram> GetApplicationsStores(List<MobileProgram> apps)
        {
            foreach (var app in apps)
            {
                bool result = _storage.TryAdd(app.ApplicationId, app);
                apps.Add(app);
            }
            return apps;
        }




    }
}
