﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PushUp.Model.ConcurrentDictionaryStorage
{
    /// <summary>
    /// Синтетическое усложнение кода адаптером, чтобы потом мокнуть зависимость коллекцию записей. 
    /// И проверить методы другой зависимости.
    /// </summary>
    public class ConcurrentDictionaryAdapter : IConcurrentDictionaryAdapter
    {
        private readonly ConcurrentDictionary<string, MobileProgram> _concurrentDictionary = new ConcurrentDictionary<string, MobileProgram>();
        public MobileProgram this[string key] { get => _concurrentDictionary[key]; set => _concurrentDictionary[key] = value; }

        public IEnumerable<MobileProgram> Values { get => _concurrentDictionary.Values; }

        public MobileProgram AddOrUpdate(string key, MobileProgram record, Func<string, MobileProgram, MobileProgram> p)
        {
            return _concurrentDictionary.AddOrUpdate(key, record, p);
        }

        public void Clear()
        {
            _concurrentDictionary.Clear();
        }

        public bool TryAdd(string key, MobileProgram value)
        {
            return _concurrentDictionary.TryAdd(key, value);
        }

        public bool TryGetValue(string key, out MobileProgram record)
        {
            return _concurrentDictionary.TryGetValue(key, out record);
        }

        public bool TryRemove(string key, out MobileProgram value)
        {
            return _concurrentDictionary.TryRemove(key, out value);
        }
    }
}
